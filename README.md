# logger

Logger php app.

## Start mysql instance
`make up_mysql`

## Run Migration
`make migration`

## Load logs into the db
`make load-logs path=<path/to/log/file.txt>`

## Generate the reports
`make report`

When running the report command the report type (consumer, service or latency) and path (dir where to save the report) will be asked.