<?php

namespace App\Commands;

use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

use DB;

class LoadLogs extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'load
                            {path : The file path to the log file }';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Load logs from a .txt file into a mysql table.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = $this->argument('path');

        if($this->validateFile($path)) {
            $this->seed($path);
            die;
        }

        $this->info("file $path not found");
    }

    private function validateFile($filePath) {
        if (file_exists($filePath) && is_readable($filePath)) {
            return true;
        }

        return false;
    }

    private function seed($filePath) {
        $fileResource = fopen($filePath, 'r');
        $count = 0;

        while(($line = fgets($fileResource)) !== false) {
            $json = json_decode($line);

            DB::table('logs')->insert([
                'consumer'=> $json->authenticated_entity->consumer_id->uuid,
                'service'=> $json->service->name,
                'request'=> $json->latencies->request,
                'kong'=> $json->latencies->kong,
                'proxy'=> $json->latencies->proxy,
            ]);

            $count += 1;
        }

        fclose($fileResource);

        $this->info("$count logs saved.");
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule)
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
