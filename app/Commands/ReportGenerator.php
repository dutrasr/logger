<?php

namespace App\Commands;

use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;
use DB;
use Storage;

class ReportGenerator extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'report';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Request by consumer report.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $reportType = $this->choice('Report type?', ['consumer', 'service', 'latency']);
        $reportPath = $this->ask('Directory to save report?');

        if (!$this->isPathValid($reportPath)) {
            $this->info("path $reportPath is not writeable or do not exist!");

            die;
        }

        $report = null;

        switch($reportType) {
            case 'consumer':
                $report = $this->consumersRequests();
            break;

            case 'service':
                $report = $this->servicesRequests();
            break;

            case 'latency':
                $report = $this->latencyAverage();
            break;
        }

        if ($report) {
            $this->generateReport($report, $reportType, $reportPath);
        }
    }

    private function isPathValid($path) {
        if (is_writeable($path)) {
            return true;
        }

        return false;
    }

    private function consumersRequests() {
        return DB::table('logs')
            ->select(
                'consumer',
                DB::raw(('count(consumer) as count_consumer')))
            ->groupBy('consumer')
            ->get();
    }

    private function servicesRequests() {
        return DB::table('logs')
            ->select(
                'service',
                DB::raw(('count(service) as count_service')))
            ->groupBy('service')
            ->get();
    }

    private function latencyAverage() {
        return DB::table('logs')
            ->select(
                'service',
                DB::raw(('AVG(request) as avg_request')),
                DB::raw(('AVG(kong) as avg_kong')),
                DB::raw(('AVG(proxy) as avg_proxy')))
            ->groupBy('service')
            ->get();
    }

    private function generateReport($report, $reportType, $reportPath) {
        $array = get_object_vars($report[0]);
        $columns = array_keys($array);

        $handler = fopen("$reportPath/$reportType.csv", 'w');

        fputcsv($handler, $columns);

        foreach($report as $row) {
            fputcsv($handler, get_object_vars($row));
        }

        fclose($handler);
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
