.PHONY: up_mysql migration load-logs report

up_mysql:
	docker-compose up -d

migration:
	php logger migrate

load-logs:
	php logger load $(path)

report:
	php logger report
